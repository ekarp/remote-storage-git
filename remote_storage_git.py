#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse, atexit, config, contextlib, logging, os, shlex, shutil, \
    stat, subprocess, sys, tarfile, tempfile

REMOTE_REPO_BRANCH = 'master'
ROOT_DIR_NAME = 'git_root'

logging.basicConfig(level=logging.ERROR)
LOGGER = logging.getLogger(__name__)

# Python only makes these available on Unix in the os module. For
# cross-platform usage, we define them here.
EX_SOFTWARE = 70
EX_USAGE = 64
EX_OK = 0

class RunError(Exception):
    """Error when the run() command exits with non-zero status."""

    def __init__(self, cmd, stdout, stderr, returncode):
        self.__cmd = cmd
        self.__stdout = stdout
        self.__stderr = stderr
        self.__returncode = returncode

    def __str__(self):
        r = ['Command failed: {}'.format(
            ' '.join(shlex.quote(c) for c in self.__cmd)),
             'Return code: {}'.format(self.__returncode),
             'STDOUT: {}'.format(self.__stdout.decode('utf-8')),
             'STDERR: {}'.format(self.__stderr.decode('utf-8'))]
        return '\n'.join(r)


@contextlib.contextmanager
def tempfilename(**kwargs):
    """Creates a temporary file (that deletes itself on context exit). Does
    so in a manner that is safe on both Windows and Linux."""
    assert 'delete' not in kwargs
    kwargs['delete'] = False
    f = tempfile.NamedTemporaryFile(**kwargs)
    f.close()
    try:
        yield f.name
    finally:
        os.remove(f.name)


def force_rmtree(dirname):
    """Recursively deletes the directory, marking read-only files as writeable
    to ensure they can be deleted (Windows complains otherwise)."""
    def onerror(func, fname, exc_info):
        os.chmod(fname, stat.S_IWRITE)
        os.remove(fname)
    shutil.rmtree(dirname, onerror=onerror)


def run(args, stdin=None):
    """Runs the commands, with optional stdin."""
    p = subprocess.Popen(args, stdout=subprocess.PIPE,
                         stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(stdin)
    if p.returncode != EX_OK:
        raise RunError(args, stdout, stderr, p.returncode)
    return stdout, stderr


def gpg_decrypt(in_fname, out_fname, key):
    """Decrypts the file contents from in_fname into out_fname using the
    key."""
    LOGGER.info('Running GPG decrypt')
    return __gpg(in_fname, out_fname, key.encode(), ['--decrypt'])


def gpg_encrypt(in_fname, out_fname, key):
    """Encrypts the file contents from in_fname into out_fname using the
    key."""
    LOGGER.info('Running GPG encrypt')
    return __gpg(in_fname, out_fname, key.encode(),
                 ['--symmetric',
                  '--cipher-algo', 'AES256',
                  '--digest-algo', 'SHA512', '--s2k-digest-algo',
                  'SHA512', '--compression-algo', 'BZIP2'])


def __gpg(in_fname, out_fname, stdin, other_args):
    """Runs a GPG command on the input/output files, with the given STDIN."""
    # --batch needed to prevent GPG from writing directly to the TTY.
    # --yes needed to automatically overwrite the out_fname.
    args = ['gpg', '--batch', '--yes', '--passphrase-fd', '0',
            '--output', out_fname]
    args.extend(other_args)
    args.append(in_fname)
    run(args, stdin=stdin)


def tar_dir(in_dir, out_fname):
    """Tars the directory into the output filename."""
    LOGGER.info('Tarring directory')
    tarred_file = tarfile.TarFile(out_fname, mode='w')
    tarred_file.add(in_dir, arcname=ROOT_DIR_NAME)
    tarred_file.close()


def untar_dir(in_fname, out_dir):
    """Untars the file into the output directory."""
    LOGGER.info('Untarring files')
    tarred_file = tarfile.TarFile(in_fname)
    tarred_file.extractall(path=out_dir)
    tarred_file.close()


def __download_decrypt(key, out_fname, service):
    """Download and decrypt the file into the output filename."""
    with tempfilename() as fname:
        LOGGER.info('Downloading repository')
        service.download_repo_file(fname)
        gpg_decrypt(fname, out_fname, key)


def __download_decrypt_unpack(key, dirname, service):
    """Download and unpack the repository into the directory."""
    with tempfilename(suffix='.tar') as tar_fname:
        __download_decrypt(key, tar_fname, service)
        untar_dir(tar_fname, dirname)


def __pack_encrypt_upload(key, tmp_dir, service):
    """Tars the files, encrypts it, then uploads it."""
    with tempfilename(suffix='.tar') as tar_fname:
       tar_dir(tmp_dir, tar_fname)
       __encrypt_upload(key, tar_fname, service)


def __encrypt_upload(key, in_fname, service):
    """Encrypts the file, then uploads it."""
    with tempfilename() as fname:
        gpg_encrypt(in_fname, fname, key)
        LOGGER.info('Uploading repository')
        service.upload_repo_file(fname)


def pull(key, service):
    """Performs a "pull" by downloading the remote git repo and issuing
    a pull against it."""
    LOGGER.info('Creating a temporary directory for "pull" command')
    tmp_dir = tempfile.mkdtemp()
    try:
        __download_decrypt_unpack(key, tmp_dir, service)
        LOGGER.info('Running "git pull" on repository at {}'.format(
            tmp_dir))
        stdout, stderr = run(['git', 'pull',
                              os.path.join(tmp_dir, ROOT_DIR_NAME),
                              REMOTE_REPO_BRANCH])
        print(stdout.decode('utf-8'))
        print(stderr.decode('utf-8'), file=sys.stderr)
    finally:
        LOGGER.info('Deleting temporary directory')
        force_rmtree(tmp_dir)


def push(key, service):
    LOGGER.info('Creating a temporary directory for "push" command')
    tmp_dir = tempfile.mkdtemp()
    try:
        __download_decrypt_unpack(key, tmp_dir, service)
        LOGGER.info('Running "git push" on repository at {}'.format(
            tmp_dir))
        tmp_repo_dir = os.path.join(tmp_dir, ROOT_DIR_NAME)
        stdout, stderr = run(['git', 'push', tmp_repo_dir,
                              REMOTE_REPO_BRANCH])
        print(stdout.decode('utf-8'))
        print(stderr.decode('utf-8'), file=sys.stderr)
        __pack_encrypt_upload(key, tmp_repo_dir, service)
    finally:
        LOGGER.info('Deleting temporary directory')
        force_rmtree(tmp_dir)


def init(key, service):
    """Creates a new repository on the remote storage, if one does not already
    exist."""
    LOGGER.info('Checking if the repository file exists.')
    if service.repo_file_exists():
        raise ValueError('A repo already exists, please run "destroy" if you want '
                         'to make a new one.')
    tmp_dir = tempfile.mkdtemp()
    cwd = os.getcwd()
    try:
        os.chdir(tmp_dir)
        run(['git', 'init', '--bare'])
        __pack_encrypt_upload(key, tmp_dir, service)
    finally:
        os.chdir(cwd)
        LOGGER.info('Deleting temporary directory')
        force_rmtree(tmp_dir)


def destroy(key, service):
    """Destroys the remote repository, after issuing a warning."""
    LOGGER.info('Running the destroy command')
    print("WARNING!!!!! You are able to blow away the repository!")
    print("If you really want to proceed, type 'I MEAN IT' (without quotes)")
    answer = input("> ")
    if answer.strip() != "I MEAN IT":
        print("You did not really mean it. The repo will not be destroyed.")
        return
    LOGGER.info('Checking if repository exists')
    if not service.repo_file_exists():
        raise ValueError('No repository exists, cannot destroy it.')
    LOGGER.info('Deleting the repository file')
    service.delete_repo_file()


def print_usage_exit(prog_name):
    print('%s %s' % (os.path.basename(prog_name),
                     '|'.join(COMMANDS)), file=sys.stderr)
    sys.exit(EX_USAGE)


COMMANDS = {'push': push,
            'pull': pull,
            'init': init,
            'destroy': destroy}
STORAGE_SYSTEMS = ('gdrive', 's3')


PARSER = argparse.ArgumentParser(description='Interact with a Git repository '
                                 'on remote storage.')
PARSER.add_argument('--storage', type=str, default='gdrive',
                    help='the storage system to use. valid values are: {}'.format(
                        ', '.join(STORAGE_SYSTEMS)))
PARSER.add_argument('command', type=str,
                    help='the command to run. valid values are: {}'.format(
                        ', '.join(COMMANDS)))
PARSER.add_argument('--config', type=str, default=config.CONFIG_FILE,
                    help='location of the configuration')
PARSER.add_argument('--verbose', action='store_const', const=True, default=False,
                    help='prints verbose logging information')


if __name__ == '__main__':
    args = PARSER.parse_args()
    if args.verbose:
        LOGGER = logging.getLogger(__name__)
        LOGGER.setLevel(logging.INFO)
    cmd = args.command
    LOGGER.info('Reading config file: {}'.format(args.config))
    cfg = config.read(args.config)
    key = cfg.get('main', 'key')
    LOGGER.info('Loading storage system interface: {}'.format(args.storage))
    if args.storage == 's3':
        # Lazy load in case the user does not use this particular system,
        # and does not want to install the libraries.
        import s3
        service = s3.Service(**dict(cfg.items(args.storage)))
    else:
        assert args.storage == 'gdrive'
        import gdrive
        service = gdrive.Service(**dict(cfg.items(args.storage)))
    try:
        COMMANDS[cmd](key, service)
    except Exception as e:
        print(str(e), file=sys.stderr)
        sys.exit(EX_SOFTWARE)
