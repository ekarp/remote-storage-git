# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import boto
from boto.s3.connection import S3Connection
import service

class Service(object):
    """Service for connecting to a remote repository on Amazon S3.
    Note that I have *not* tested this class."""

    def __init__(self, remote_filename, bucket, access_key, private_key):
        super(Service, self).__init__()
        self.__s3_filename = remote_filename
        self.__default_bucket = bucket
        self.__access_key = access_key
        self.__private_key = private_key
    
    def download_repo_file(self, output_fname):
        self.__get_repo_key(self.__s3_filename).get_contents_to_filename(
            output_fname)

    def upload_repo_file(self, input_fname):
        self.__get_repo_key(self.__s3_filename).set_contents_from_filename(
            input_fname)

    def repo_file_exists(self):
        bucket = self.__get_repo_bucket()
        return self.__s3_filename in (k.key for k in bucket.list())

    def delete_repo_file(self):
        self.__get_repo_bucket().delete_key(self.__s3_filename)

    def __get_repo_key(self, keyname):
        k = boto.s3.key.Key(self.__get_repo_bucket())
        k.key = keyname
        return k

    def __get_repo_bucket(self):
        conn = S3Connection(self.__access_key, self.__private_key)
        return conn.get_bucket(self.__default_bucket)
