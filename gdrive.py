#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from apiclient import errors
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient.http import MediaFileUpload
from apiclient.http import MediaIoBaseDownload
import argparse, config, hashlib, httplib2, io, os, service, sys

MIMETYPE = 'application/octet-stream'
SCOPES = 'https://www.googleapis.com/auth/drive.file'

class Service(service.Service):
    """Service for connecting to a remote repository on Google Drive.
    Tested on January 8, 2016."""

    def __init__(self, filename_id, client_secret_json, application_name,
                 credentials_filename, remote_filename):
        super(Service, self).__init__()
        self.__filename_id = filename_id
        self.__client_secret_json = self.__expand_file(client_secret_json)
        self.__application_name = application_name
        self.__credentials_filename = self.__expand_file(credentials_filename)
        self.__remote_filename = remote_filename
        self.__credentials = self.__get_credentials()
        http = self.__credentials.authorize(httplib2.Http())
        self.__service = discovery.build('drive', 'v3', http=http)

    def __get_credentials(self):
        """Gets user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        This code is based on the Google documentation online.

        Returns:
          Credentials, the obtained credential.
        """
        store = Storage(self.__credentials_filename)
        credentials = store.get()
        if not credentials or credentials.invalid:
            # On Windows, the argparser tries to interpret the command line
            # arguments to the program as arguments to the work flow.
            # Temporarily remove them for flag parsing.
            flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args([])
            flow = client.flow_from_clientsecrets(
                self.__client_secret_json, SCOPES)
            flow.user_agent = self.__application_name
            credentials = tools.run_flow(flow, store, flags)
        return credentials

    def generate_file_id(self):
        """Generates a new file ID that can be used with the repository."""
        f = self.__service.files().generateIds(count=1).execute()
        return f['ids'][0]

    def download_repo_file(self, output_fname):
        """Download the latest version of the repository file from the remote
        location into the output file. It is still up to the caller
        to decrypt and unpack it."""
        request = self.__service.files().get_media(fileId=self.__filename_id)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while not done:
            _, done = downloader.next_chunk()
        fh.seek(0)
        with open(output_fname, 'wb') as f:
            f.write(fh.read())

    def upload_repo_file(self, input_fname):
        """Upload the file to the remote location and make it the new
        repository file."""
        media_body = MediaFileUpload(input_fname, mimetype=MIMETYPE,
                                     resumable=True)
        body = {
            'name': self.__remote_filename,
            'mimeType': MIMETYPE,
            'writersCanShare': False,
            'viewersCanCopyContent': False,
        }
        if self.repo_file_exists():
            file = self.__service.files().update(
                body=body,
                fileId=self.__filename_id,
                media_body=media_body).execute()
        else:
            body['id'] = self.__filename_id
            file = self.__service.files().create(
                body=body,
                media_body=media_body).execute()
        if file['id'] != self.__filename_id:
            raise ValueError('Google Drive returned a different file ID ({})'
                             'than the one it was supposed to use ({}).'.format(
                                 file['id'], self.__filename_id))
        file = self.__service.files().get(fileId=self.__filename_id,
                                          fields='md5Checksum').execute()
        self.__check_md5(input_fname, file['md5Checksum'])

    def repo_file_exists(self):
        """Returns true/false if the file exists."""
        try:
            file = self.__service.files().get(
                fileId=self.__filename_id).execute()
            return True
        except errors.HttpError as e:
            if "File not found: {}".format(self.__filename_id) in str(e):
                return False
            raise e

    def delete_repo_file(self):
        self.__service.files().delete(fileId=self.__filename_id).execute()

    @staticmethod
    def __expand_file(filename):
        """Replaces ~ with user directories and changes relative files to be relative
        to this script."""
        base_dir = os.path.dirname(__file__)
        return os.path.join(base_dir, os.path.expanduser(filename))

    @staticmethod
    def __check_md5(fname, online_md5sum):
        """Checks that the filename's md5sum on disk matches the online md5sum."""
        with open(fname, 'rb') as f:
            md5sum = hashlib.md5(f.read()).hexdigest()
            if md5sum != online_md5sum:
                raise ValueError('Google Drive returned a different md5sum for '
                                 'the repository file than the one on disk.\n'
                                 'On disk: {}\nOn Google Drive: {}'.format(
                                     md5sum, online_md5sum))


PARSER = argparse.ArgumentParser(description='Generates a file ID in Google Drive '
                                 'for the repository.')
PARSER.add_argument('--config', type=str, default=config.CONFIG_FILE,
                    help='location of the configuration')


if __name__ == '__main__':
    args = PARSER.parse_args()
    cfg = config.read(args.config)
    service = Service(**dict(cfg.items('gdrive')))
    print('Generated file ID: {}'.format(service.generate_file_id()))
