Remote Storage to Git Repository
================================

A Python script that turns a cloud based storage system into a Git repository. Since the entire repository is uploaded and / or downloaded for each command, it is only recommended for small repos and for single users. My motivation for writing this script was to have a simple way to keep my Git repository in sync across my multiple machines, while having everything encrypted when in the cloud and without having to run a separate server.

Please see the [home page](https://ekarp.com/projects/remote-storage-git/) for documentation.