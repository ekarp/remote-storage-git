#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import contextlib, os, remote_storage_git, service, shutil, stat, tempfile, \
    unittest

@contextlib.contextmanager
def chdir(c):
    """Changes the working directory, but restores the old working directory
    after exit."""
    cwd = os.getcwd()
    try:
        os.chdir(c)
        yield
    finally:
        os.chdir(cwd)


class LocalService(service.Service):
    """A class that implements the interface for the remote storage interface,
    but uses local storage."""

    def __init__(self, dirname):
        super(LocalService, self).__init__()
        self.__dirname = dirname

    def filename(self):
        """Returns the path to the file being stored."""
        return os.path.join(self.__dirname, 'repo')

    def download_repo_file(self, output_fname):
        shutil.copy(self.filename(), output_fname)

    def upload_repo_file(self, input_fname):
        shutil.copy(input_fname, self.filename())

    def repo_file_exists(self):
        return os.path.isfile(self.filename())

    def delete_repo_file(self):
        os.remove(self.filename())


def change_byte(i, filename):
    """Changes the i-th byte in the file."""
    with open(filename, 'rb') as f:
        b = list(f.read())
    b[i] = (b[i] + 1) % 256
    with open(filename, 'wb') as f:
        f.write(bytes(b))


class RemoteStorageGitUtilityTest(unittest.TestCase):
    """Tests for the helper functions force_rmtree and tempfilename."""

    def testTempFilenameDeletesAfterUse(self):
        """Verify the file is removed after use."""
        with remote_storage_git.tempfilename() as fname:
            name = fname
        self.assertFalse(os.path.exists(name))

    def testTempFilenameDeletesWithException(self):
        """If an exception is thrown, the temp file is still deleted on cleanup."""
        try:
            with remote_storage_git.tempfilename() as fname:
                name = fname
                raise ValueError("error")
        except ValueError:
            self.assertFalse(os.path.exists(name))
            return
        self.fail("Should have caught exception")

    def testTempfilenameArgs(self):
        """Pass in suffix and prefix to the tempfilename() method."""
        with remote_storage_git.tempfilename(prefix="foo1", suffix="bar2") as fname:
            self.assertTrue(os.path.basename(fname).startswith("foo1"))
            self.assertTrue(fname.endswith("bar2"))

    def testForceRmTreeWorksWithReadOnly(self):
        """Verify that force_rmtree() works even if files are marked as read-only."""
        directory = tempfile.mkdtemp()
        fname = os.path.join(directory, "test_file")
        with open(fname, 'w') as f:
            f.write("test")
        os.chmod(fname, stat.S_IREAD)
        remote_storage_git.force_rmtree(directory)


class RemoteStorageGitTest(unittest.TestCase):
    """Tests the remote storage git script by instantiating two local clients
    and testing with a fake remote storage system (that also uses local storage)."""

    def setUp(self):
        self.client1_dir = tempfile.mkdtemp()
        with chdir(self.client1_dir):
            remote_storage_git.run(['git', 'init'])

        self.client2_dir = tempfile.mkdtemp()
        with chdir(self.client2_dir):
            remote_storage_git.run(['git', 'init'])

        self.local_storage_dir = tempfile.mkdtemp()
        self.local_service = LocalService(self.local_storage_dir)

    def tearDown(self):
        remote_storage_git.force_rmtree(self.client1_dir)
        remote_storage_git.force_rmtree(self.client2_dir)
        remote_storage_git.force_rmtree(self.local_storage_dir)

    def testCompresses(self):
        """The GPG algorithm includes a compression step, so the uploaded file
        size should be much less than the original size, if the text is
        compressible."""
        size = 1000 * 1000 * 10  # 10mb.
        with open(os.path.join(self.client1_dir, 'test.txt'), 'wb') as f:
            f.write((' ' * size).encode())
        with chdir(self.client1_dir):
            remote_storage_git.run(['git', 'add', 'test.txt'])
            remote_storage_git.run(['git', 'commit', '-a', '-m', '.'])
            remote_storage_git.init('fake key', self.local_service)
            remote_storage_git.push('fake key', self.local_service)
            self.assertTrue(os.path.getsize(self.local_service.filename()) <
                            size)

    def testWrongKey(self):
        remote_storage_git.init('fake key', self.local_service)
        with chdir(self.client1_dir):
            self.assertRaises(remote_storage_git.RunError,
                              remote_storage_git.pull,
                              'fake key2',
                              self.local_service)

    def testClientSync(self):
        """Verify that the changes from one client are propagated to the
        other one."""
        os.makedirs(os.path.join(self.client1_dir, 'foo', 'bar'))
        with chdir(os.path.join(self.client1_dir, 'foo', 'bar')):
            with open('test.txt', 'wb') as f:
                f.write(('hello').encode())
            remote_storage_git.run(['git', 'add', 'test.txt'])
            remote_storage_git.run(['git', 'commit', '-a', '-m', '.'])
            remote_storage_git.init('fake key', self.local_service)
            remote_storage_git.push('fake key', self.local_service)
        with chdir(self.client2_dir):
            self.assertFalse(os.path.isfile('test.txt'))
            remote_storage_git.pull('fake key', self.local_service)
            self.assertTrue(os.path.isdir('foo'))
            self.assertEqual(['bar'], os.listdir('foo'))
            self.assertEqual(['test.txt'],
                             os.listdir(os.path.join('foo', 'bar')))
            with open(os.path.join('foo', 'bar', 'test.txt'), 'r') as f:
                self.assertEqual('hello', f.read())

    def testTransientError(self):
        """Flip bits in the "remotely" stored version and verify that the
        script detects the error."""
        with open(os.path.join(self.client1_dir, 'test.txt'), 'wb') as f:
            f.write(' '.encode())
        with chdir(self.client1_dir):
            remote_storage_git.run(['git', 'add', 'test.txt'])
            remote_storage_git.run(['git', 'commit', '-a', '-m', '.'])
            remote_storage_git.init('fake key', self.local_service)
            remote_storage_git.push('fake key', self.local_service)

            with remote_storage_git.tempfilename() as copyname:
                shutil.copy(self.local_service.filename(), copyname)
                self.assertTrue(os.path.getsize(self.local_service.filename()) >
                                30)
                # Flip 10 bytes near the beginning, middle, and end, individually.
                for i in range(10):
                    # Flip at the beginning.
                    change_byte(i, self.local_service.filename())
                    self.assertRaises(remote_storage_git.RunError,
                                      remote_storage_git.pull,
                                      'fake key', self.local_service)
                    shutil.copy(copyname, self.local_service.filename())

                    # Flip near the middle.
                    offset = int(os.path.getsize(self.local_service.filename()) /
                                 2)
                    change_byte(i + offset, self.local_service.filename())
                    self.assertRaises(remote_storage_git.RunError,
                                      remote_storage_git.pull,
                                      'fake key', self.local_service)
                    shutil.copy(copyname, self.local_service.filename())

                    # Flip at the end.
                    offset = os.path.getsize(self.local_service.filename()) - 10
                    change_byte(i + offset, self.local_service.filename())
                    self.assertRaises(remote_storage_git.RunError,
                                      remote_storage_git.pull,
                                      'fake key', self.local_service)
                    shutil.copy(copyname, self.local_service.filename())


if __name__ == '__main__':
    unittest.main()
