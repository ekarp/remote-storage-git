# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

class Service(object):
    """Interface for interacting with the remote repository."""

    def download_repo_file(self, output_fname):
        """Download the latest version of the repository file from the remote
        location into the output filename. It is still up to the
        caller to decrypt and unpack it."""
        raise NotImplemented('implement me')

    def upload_repo_file(self, input_fname):
        """Upload the file to the remote location and make it the new
        repository file."""
        raise NotImplemented('implement me')

    def repo_file_exists(self):
        """Returns true/false if the repo already exists on the remote
        storage system."""
        raise NotImplemented('implement me')

    def delete_repo_file(self):
        """Deletes the repo file from the remote storage system."""
        raise NotImplemented('implement me')
